<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;
use DB;
use App\Summary;
use Auth;

class SummaryController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    public function create()
    {
        return view('summary.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'subject' => 'required',
            'content' => 'required',
        ]);

        $summary = Summary::create([
            "subject" => $request["subject"],
            "content" => $request["content"],
            "user_id" => Auth::id()
        ]);

        Alert::success('Success', 'New Summary Has Been Added');

        return redirect('/summary');
    }

    public function index()
    {
        $user = Auth::user();

        $summary = $user->summaries;

        return view('summary.index', compact('summary'));
    }

    public function show($id)
    {
        $summary = Summary::find($id);
        return view('summary.show', compact('summary'));
    }

    public function edit($id)
    {
        $summary = Summary::find($id);
        return view('summary.edit', compact('summary'));
    }

    public function update($id, Request $request){
        $request->validate([
            'subject' => 'required',
            'content' => 'required',
        ]);

        $update = Summary::where('id', $id)->update([
            "subject" => $request["subject"],
            "content" => $request["content"],
        ]);

        return redirect('/summary');
    }

    public function destroy($id){
        Summary::destroy($id);
        return redirect('/summary');
    }
}
