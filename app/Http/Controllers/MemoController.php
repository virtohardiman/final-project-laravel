<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;
use DB;
use App\Memo;
use Auth;

class MemoController extends Controller
{
        public function __construct() {
            $this->middleware('auth');
    }

    public function create()
    {
        return view('memo.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'reminder' => 'required',
        ]);

        $memo = Memo::create([
            "title" => $request["title"],
            "reminder" => $request["reminder"],
            "user_id" => Auth::id()
        ]);

        Alert::success('Success', 'New Memo Has Been Added');

        return redirect('/memo');
    }

    public function index()
    {
        $user = Auth::user();

        $memo = $user->memos;

        return view('memo.index', compact('memo'));
    }

    public function show($id)
    {
        $memo = Memo::find($id);
        return view('memo.show', compact('memo'));
    }

    public function edit($id)
    {
        $memo = Memo::find($id);
        return view('memo.edit', compact('memo'));
    }

    public function update($id, Request $request){
        $request->validate([
            'title' => 'required',
            'reminder' => 'required',
        ]);

        $update = Memo::where('id', $id)->update([
            "title" => $request["title"],
            "reminder" => $request["reminder"]
        ]);

        return redirect('/memo');
    }

    public function destroy($id){
        Memo::destroy($id);
        return redirect('/memo');
    }
}
