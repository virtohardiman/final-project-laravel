<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;
use DB;
use App\Report;
use Auth;

class ReportController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    public function create()
    {
        return view('report.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'description' => 'required',
            'screenshot' => 'required|mimes:jpeg,png,jpg',
        ]);

        $photo = $request->screenshot;
        $name_img = time(). ' - ' . $photo->getClientOriginalName();

        $report = Report::create([
            "title" => $request["title"],
            "description" => $request["description"],
            "screenshot" => $name_img,
            "user_id" => Auth::id()
        ]);

        $photo->move('img',$name_img);

        Alert::success('Success', 'New Report Has Been Added');

        return redirect('/report');
    }

    public function index()
    {
        $user = Auth::user();

        $report = $user->reports;

        return view('report.index', compact('report'));
    }

    public function show($id)
    {
        $report = Report::find($id);
        return view('report.show', compact('report'));
    }

    public function edit($id)
    {
        $report = Report::find($id);
        return view('report.edit', compact('report'));
    }

    public function update($id, Request $request){
        $request->validate([
            'title' => 'required',
            'description' => 'required',
        ]);

        $update = Report::where('id', $id)->update([
            "title" => $request["title"],
            "description" => $request["description"]
        ]);

        return redirect('/report');
    }

    public function destroy($id){
        Report::destroy($id);
        return redirect('/report');
    }
}
