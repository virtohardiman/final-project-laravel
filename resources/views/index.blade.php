@extends('layout.master')

@section('navbar')
<li><a href="#">Login</a></li>
<li class="active"><a href="#">Home</a></li>
<li><a href="#">Post</a></li>
@endsection

@section('title')
    Find Best Article Here!
@endsection

@section('content')
@forelse ($article as $key=>$value)
<div class="row">
    <div class="col-lg-12 col-md-12">
        <div class="categories__post__item categories__post__item--large">
            <div class="categories__post__item__pic set-bg">
                <h1>{{$value->thumbnail}}</h1>
            </div>
            <div class="categories__post__item__text">
                <ul class="post__label--large">
                    <li>Vegan</li>
                    <li>Desserts</li>
                </ul>
                <h3><a href="#">{{$value->title}}</a></h3>
                <ul class="post__widget">
                    <li>by <span>Admin</span></li>
                    <li>3 min read</li>
                    <li>20 Comment</li>
                </ul>
                <p>{{$value->content}}</p>
                <a href="#" class="primary-btn">Read more</a>
                <div class="post__social">
                    <span>Share</span>
                    <a href="#"><i class="fa fa-facebook"></i> <span>82</span></a>
                    <a href="#"><i class="fa fa-twitter"></i> <span>24</span></a>
                    <a href="#"><i class="fa fa-envelope-o"></i> <span>08</span></a>
                </div>
            </div>
        </div> 
    </div>
</div>
@empty
    <h1>No data</h1>
@endforelse 
@endsection