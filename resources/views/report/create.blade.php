@extends('layout.master')

@section('navbar')
<li class="active"><a href="#">Report</a></li>
<li><a href="/summary">Summary</a></li>
<li><a href="/memo">Memo</a></li>
@endsection

@section('title')
    Submit Financial Report (Image Can't be Edited)
@endsection

@section('content')
<div>
    <form action="/report" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
            <label for="title">Title</label>
            <input type="text" class="form-control" name="title" value="{{old('title', '')}}" id="title" placeholder="Insert Title">
            @error('title')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="description">Description</label>
            <input type="text" class="form-control" name="description" value="{{old('description', '')}}" id="description" placeholder="Insert Report Description">
            @error('description')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="screenshot">Screenshot Image</label>
            <input type="file" class="form-control" name="screenshot" id="screenshot">
            @error('screenshot')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Submit Financial Report</button>
    </form>
</div>
@endsection