@extends('layout.master')

@section('navbar')
<li class="active"><a href="#">Report</a></li>
<li><a href="/summary">Summary</a></li>
<li><a href="/memo">Memo</a></li>
@endsection

@section('title')
    Show Report Id: {{$report->id}}
@endsection

@section('content')
    <div>
        <form action="/report/{{$report->id}}" method="POST">
            @csrf
            @method('put')
            <div class="form-group">
                <label for="title">Title</label>
                <input type="text" class="form-control" value="{{$report->title}}" name="title" value="{{old('title', '')}}" id="title" placeholder="Insert Title">
                @error('title')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="content">Description</label>
                <input type="text" class="form-control" value="{{$report->description}}" name="description" value="{{old('description', '')}}" id="description" placeholder="Insert Report Description">
                @error('description')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Edit</button>
        </form>
    </div>
@endsection