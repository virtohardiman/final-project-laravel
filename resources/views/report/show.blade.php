@extends('layout.master')

@section('navbar')
<li class="active"><a href="#">Report</a></li>
<li><a href="/summary">Summary</a></li>
<li><a href="/memo">Memo</a></li>
@endsection

@section('title')
    Show Report Id: {{$report->id}}
@endsection

@section('content')
    <h4>{{$report->title}}</h4>
    <p>{{$report->description}}</p>
    <p>Image Name: {{$report->screenshot}}</p>
@endsection