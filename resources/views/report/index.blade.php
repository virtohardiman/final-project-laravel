@extends('layout.master')

@section('navbar')
<li class="active"><a href="#">Report</a></li>
<li><a href="/summary">Summary</a></li>
<li><a href="/memo">Memo</a></li>
@endsection

@section('title')
    Welcome {{ Auth::user()->name }}, Here is Your Financial Report List
@endsection

@section('content')
<a href="/report/create" class="btn btn-primary my-2">Add Report</a>
<table class="table">
    <thead class="thead-light">
      <tr>
        <th scope="col">#</th>
        <th scope="col">Title</th>
        <th scope="col">Description</th>
        <th scope="col">Report Screenshot</th>
        <th scope="col">Actions</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($report as $key=>$value)
            <tr>
                <td>{{$key + 1}}</th>
                <td>{{$value->title}}</td>
                <td>{{$value->description}}</td>
                <td><img src="{{ asset('img/'.$value->screenshot)}}" style="height: 100px;"></td>
                <td>
                    <form action="/report/{{$value->id}}" method="POST">
                        <a href="/report/{{$value->id}}" class="btn btn-info">Show</a>
                        <a href="/report/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                        @csrf
                        @method('DELETE')
                        <input type="submit" class="btn btn-danger my-1" value="Delete">
                    </form>
                </td>
            </tr>
        @empty
            <tr colspan="3">
                <td>No data</td>
            </tr>  
        @endforelse              
    </tbody>
</table>
@endsection