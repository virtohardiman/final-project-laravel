@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <h1>WELCOME to ReSuMo {{$name}}!</h1>
                    <img src="{{asset('img/logo-crop.png')}}" alt="" height="200" width="400"> <br>
                    You are logged in! Click <a href="/report" style="color:tomato;">here</a> to continue
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
