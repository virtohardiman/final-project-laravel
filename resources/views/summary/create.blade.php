@extends('layout.master')

@section('navbar')
<li><a href="/report">Report</a></li>
<li class="active"><a href="#">Summary</a></li>
<li><a href="/memo">Memo</a></li>
@endsection

@section('title')
    Submit Financial Summary
@endsection

@section('content')
<div>
    <form action="/summary" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
            <label for="subject">Subject</label>
            <input type="text" class="form-control" name="subject" value="{{old('subject', '')}}" id="subject" placeholder="Insert Subject">
            @error('subject')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="content">Content</label>
            <textarea name="content" class="form-control"></textarea>
            @error('content')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Submit Financial Summary</button>
    </form>
</div>

@push('scripts')
<script src="https://cdn.tiny.cloud/1/gs2j46ngbvi6yy0cwzrig8eck6pwbsnpsr48vseata0hp56n/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
<script>
    tinymce.init({
      selector: 'textarea',
      plugins: 'a11ychecker advcode casechange formatpainter linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tinycomments tinymcespellchecker',
      toolbar: 'a11ycheck addcomment showcomments casechange checklist code formatpainter pageembed permanentpen table',
      toolbar_mode: 'floating',
      tinycomments_mode: 'embedded',
      tinycomments_author: 'Author name',
   });
  </script>
@endpush

@endsection