@extends('layout.master')

@section('navbar')
<li><a href="/report">Report</a></li>
<li class="active"><a href="#">Summary</a></li>
<li><a href="/memo">Memo</a></li>
@endsection

@section('title')
    Show Summary Id: {{$summary->id}}
@endsection

@section('content')
    <h4>{{$summary->subject}}</h4>
    <p>{{!!$summary->content!!}}</p>
@endsection