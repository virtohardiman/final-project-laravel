@extends('layout.master')

@section('navbar')
<li><a href="/report">Report</a></li>
<li class="active"><a href="#">Summary</a></li>
<li><a href="/memo">Memo</a></li>
@endsection

@section('title')
    Show Summary Id: {{$summary->id}}
@endsection

@section('content')
    <div>
        <form action="/summary/{{$summary->id}}" method="POST">
            @csrf
            @method('put')
            <div class="form-group">
                <label for="subject">Subject</label>
                <input type="text" class="form-control" value="{{$summary->subject}}" name="subject" value="{{old('subject', '')}}" id="subject" placeholder="Insert Subject">
                @error('subject')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="content">Content</label>
                <input type="text" class="form-control" value="{{$summary->content}}" name="content" value="{{old('content', '')}}" id="content" placeholder="Insert Summary Content">
                @error('content')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Edit</button>
        </form>
    </div>
@endsection