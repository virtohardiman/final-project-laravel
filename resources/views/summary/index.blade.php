@extends('layout.master')

@section('navbar')
<li><a href="/report">Report</a></li>
<li class="active"><a href="#">Summary</a></li>
<li><a href="/memo">Memo</a></li>
@endsection

@section('title')
    Welcome {{ Auth::user()->name }}, Here is Your Summary List
@endsection

@section('content')
<a href="/summary/create" class="btn btn-primary my-2">Add Summary</a>
<table class="table">
    <thead class="thead-light">
      <tr>
        <th scope="col">#</th>
        <th scope="col">Subject</th>
        <th scope="col">Content</th>
        <th scope="col">Actions</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($summary as $key=>$value)
            <tr>
                <td>{{$key + 1}}</th>
                <td>{{$value->subject}}</td>
                <td>{{!!$value->content!!}}</td>
                <td>
                    <form action="/summary/{{$value->id}}" method="POST">
                        <a href="/summary/{{$value->id}}" class="btn btn-info">Show</a>
                        <a href="/summary/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                        @csrf
                        @method('DELETE')
                        <input type="submit" class="btn btn-danger my-1" value="Delete">
                    </form>
                </td>
            </tr>
        @empty
            <tr colspan="3">
                <td>No data</td>
            </tr>  
        @endforelse              
    </tbody>
</table>
@endsection