<!DOCTYPE html>
<html lang="zxx">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="Foodeiblog Template">
    <meta name="keywords" content="Foodeiblog, unica, creative, html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Foodeiblog | Template</title>

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Nunito+Sans:300,400,600,700,800,900&display=swap"
        rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Unna:400,700&display=swap" rel="stylesheet">

    <!-- Css Styles -->
    <link rel="stylesheet" href="{{ asset('foodeiblog/css/bootstrap.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{ asset('foodeiblog/css/font-awesome.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{ asset('foodeiblog/css/elegant-icons.css')}}" type="text/css">
    <link rel="stylesheet" href="{{ asset('foodeiblog/css/owl.carousel.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{ asset('foodeiblog/css/slicknav.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{ asset('foodeiblog/css/style.css')}}" type="text/css">

    <!-- Icons -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

    @stack('head-script')
</head>

<body>
    <!-- Page Preloder -->
    <div id="preloder">
        <div class="loader"></div>
    </div>


    <!-- Header Section Begin -->
    <header class="header">
        <div class="header__top">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-1 col-6 order-md-1 order-1">
                    <a href="/" style="color:tomato; line-height: 42px;">Quit</a>
                    </div>
                    <div class="col-lg-6 col-md-10 order-md-2 order-3">
                        <nav class="header__menu">
                            <ul>
                                @yield('navbar')
                            </ul>
                        </nav>
                    </div>
                    <div class="col-lg-3 col-md-1 col-6 order-md-3 order-2">
              
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-9 col-md col-md-9">
                    <h1 style="line-height: 150px;">@yield('title')</h1>
                </div>
                <div class="col-lg-3 col-md col-md-3">
                    <div class="header__social">
                        <a href="#"><i class="fa fa-facebook"></i></a>
                        <a href="#"><i class="fa fa-twitter"></i></a>
                        <a href="#"><i class="fa fa-youtube-play"></i></a>
                        <a href="#"><i class="fa fa-instagram"></i></a>
                        <a href="#"><i class="fa fa-envelope-o"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!-- Header Section End -->

    <!-- Container Section Begin -->
    <section class="about spad">
        <div class="container">
            @yield('content')
        </div>
    </section>
    <!-- Container Section End -->

    <!-- Footer Section Begin -->
    @include('partial.footer')
    <!-- Footer Section End -->


    <!-- Js Plugins -->
    <script src="{{ asset('foodeiblog/js/jquery-3.3.1.min.js')}}"></script>
    <script src="{{ asset('foodeiblog/js/bootstrap.min.js')}}"></script>
    <script src="{{ asset('foodeiblog/js/jquery.slicknav.js')}}"></script>
    <script src="{{ asset('foodeiblog/js/owl.carousel.min.js')}}"></script>
    <script src="{{ asset('foodeiblog/js/main.js')}}"></script>
    @stack('scripts')

    @include('sweetalert::alert')
</body>

</html>