@extends('layout.master')

@section('navbar')
<li><a href="/report">Report</a></li>
<li><a href="/summary">Summary</a></li>
<li class="active"><a href="#">Memo</a></li>
@endsection

@section('title')
    Welcome {{ Auth::user()->name }}, Here is Your Memo List
@endsection

@section('content')
<a href="/memo/create" class="btn btn-primary my-2">Add Memo</a>
<table class="table">
    <thead class="thead-light">
      <tr>
        <th scope="col">#</th>
        <th scope="col">Title</th>
        <th scope="col">Reminder</th>
        <th scope="col">Actions</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($memo as $key=>$value)
            <tr>
                <td>{{$key + 1}}</th>
                <td>{{$value->title}}</td>
                <td>{{$value->reminder}}</td>
                <td>
                    <form action="/memo/{{$value->id}}" method="POST">
                        <a href="/memo/{{$value->id}}" class="btn btn-info">Show</a>
                        <a href="/memo/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                        @csrf
                        @method('DELETE')
                        <input type="submit" class="btn btn-danger my-1" value="Delete">
                    </form>
                </td>
            </tr>
        @empty
            <tr colspan="3">
                <td>No data</td>
            </tr>  
        @endforelse              
    </tbody>
</table>
@endsection