@extends('layout.master')

@section('navbar')
<li><a href="/report">Report</a></li>
<li><a href="/summary">Summary</a></li>
<li class="active"><a href="#">Memo</a></li>
@endsection

@section('title')
    Show Memo Id: {{$memo->id}}
@endsection

@section('content')
    <div>
        <form action="/memo/{{$memo->id}}" method="POST">
            @csrf
            @method('put')
            <div class="form-group">
                <label for="title">Title</label>
                <input type="text" class="form-control" value="{{$memo->title}}" name="title" id="title" placeholder="Insert Title">
                @error('title')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="content">Reminder</label>
                <input type="text" class="form-control" value="{{$memo->reminder}}" name="reminder" id="reminder" placeholder="Insert Memo Reminder">
                @error('reminder')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Edit</button>
        </form>
    </div>
@endsection