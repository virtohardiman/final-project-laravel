@extends('layout.master')

@section('navbar')
<li><a href="/report">Report</a></li>
<li><a href="/summary">Summary</a></li>
<li class="active"><a href="#">Memo</a></li>
@endsection

@section('title')
    Show Memo Id: {{$memo->id}}
@endsection

@section('content')
    <h4>{{$memo->title}}</h4>
    <p>{{$memo->reminder}}</p>
@endsection