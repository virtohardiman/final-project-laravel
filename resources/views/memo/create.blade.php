@extends('layout.master')

@section('navbar')
<li><a href="/report">Report</a></li>
<li><a href="/summary">Summary</a></li>
<li class="active"><a href="#">Memo</a></li>
@endsection

@section('title')
    Submit Financial Report
@endsection

@section('content')
<div>
    <form action="/memo" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
            <label for="title">Title</label>
            <input type="text" class="form-control" name="title" value="{{old('title', '')}}" id="title" placeholder="Insert Title">
            @error('title')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="reminder">Reminder</label>
            <input type="text" class="form-control" name="reminder" value="{{old('reminder', '')}}" id="reminder" placeholder="Insert Reminder Message">
            @error('reminder')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Submit Memo</button>
    </form>
</div>
@endsection