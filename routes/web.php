<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// CRUD Report
Route::get('/report', 'ReportController@index');
Route::get('/report/create', 'ReportController@create');
Route::post('/report', 'ReportController@store');
Route::get('/report/{report_id}', 'ReportController@show');
Route::get('/report/{report_id}/edit', 'ReportController@edit');
Route::put('/report/{report_id}', 'ReportController@update');
Route::delete('/report/{report_id}', 'ReportController@destroy');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


// CRUD Summary
Route::get('/summary', 'SummaryController@index');
Route::get('/summary/create', 'SummaryController@create');
Route::post('/summary', 'SummaryController@store');
Route::get('/summary/{summary_id}', 'SummaryController@show');
Route::get('/summary/{summary_id}/edit', 'SummaryController@edit');
Route::put('/summary/{summary_id}', 'SummaryController@update');
Route::delete('/summary/{summary_id}', 'SummaryController@destroy');

Auth::routes();

Route::get('/summary', 'SummaryController@index')->name('summary');


// CRUD Memo
Route::get('/memo', 'MemoController@index');
Route::get('/memo/create', 'MemoController@create');
Route::post('/memo', 'MemoController@store');
Route::get('/memo/{memo_id}', 'MemoController@show');
Route::get('/memo/{memo_id}/edit', 'MemoController@edit');
Route::put('/memo/{memo_id}', 'MemoController@update');
Route::delete('/memo/{memo_id}', 'MemoController@destroy');

Auth::routes();

Route::get('/memo', 'MemoController@index')->name('memo');


Route::get('/user-policy', function(){

    $pdf = App::make('dompdf.wrapper');
    $pdf->loadHTML('<h1>User Policy</h1>
    <p>Please read this acceptable user policy ("policy", “AUP”) carefully before using the website.
    Services provided by us may only be used for lawful purposes. You agree to comply with all
    applicable laws, rules, and regulations in connection with your use of the services. Any material
    or conduct that in our judgment violates this policy in any manner may result in suspension or
    termination of the services or removal of user’s account with or without notice.</p><br>
    <h2>Prohibited use</h2>
    <p>You may not use the services to publish content or engage in activity that is illegal under
    applicable law, that is harmful to others, or that would subject us to liability, including, without
    limitation, in connection with any of the following, each of which is prohibited under this AUP:<br>
    • Phishing or engaging in identity theft<br>
    • Distributing computer viruses, worms, Trojan horses, or other malicious code<br>
    • Distributing pornography or adult related content or offering any escort services<br>
    • Promoting or facilitating violence or terrorist activities<br>
    • Infringing the intellectual property or other proprietary rights of others</p><br>
    <h2>Enforcement</h2>
    <p>Your services may be suspended or terminated with or without notice upon any violation of this
    policy. Any violations may result in the immediate suspension or termination of your account.</p><br>
    <h2>Reporting violations</h2>
    <p>To report a violation of this policy, please contact us.
    We reserve the right to change this policy at any given time, of which you will be promptly
    updated. If you want to make sure that you are up to date with the latest changes, we advise
    you to frequently visit this page.<p>
    ');
    return $pdf->stream();

});

Route::group(['prefix' => 'laravel-filemanager', 'middleware' => ['web', 'auth']], function () {
    \UniSharp\LaravelFilemanager\Lfm::routes();
});